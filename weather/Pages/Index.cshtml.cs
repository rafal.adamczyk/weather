﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using weather.Data;
using weather.Data.Dtos;
using weather.Services;

namespace weather.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IOpenWeatherService _openWeatherService;

        public IndexModel(ApplicationDbContext dbContext, IOpenWeatherService openWeatherService)
        {
            _dbContext = dbContext;
            _openWeatherService = openWeatherService;
        }

        [BindProperty, Required(ErrorMessage = "You absolutely must choose one")]
        public int ChosenCityId { get; set; }

        public string ChosenCityName { get; set; }

        public IReadOnlyCollection<SelectListItem> AvailableCities { get; set; }

        public WeatherDto Weather { get; set; }

        public void OnGet()
        {
            FetchAvailableCities();
        }

        public async Task OnPostAsync()
        {
            FetchAvailableCities();

            if (ModelState.IsValid)
            {
                ChosenCityName = _dbContext.Cities.Single(c => c.OpenWeartherId == ChosenCityId).Name;
                Weather = await _openWeatherService.GetWeather(ChosenCityId);
            }
        }

        private void FetchAvailableCities()
        {
            AvailableCities = _dbContext.Cities.AsNoTracking()
                .Select(c => new SelectListItem {Text = c.Name, Value = c.OpenWeartherId.ToString()}).ToList();
        }
    }
}
