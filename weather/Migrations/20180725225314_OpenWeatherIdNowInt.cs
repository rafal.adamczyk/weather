﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace weather.Migrations
{
    public partial class OpenWeatherIdNowInt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OpenWeartherId",
                table: "Cities",
                nullable: false,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "OpenWeartherId",
                table: "Cities",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
