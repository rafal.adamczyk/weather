﻿using System.Threading.Tasks;
using weather.Data.Dtos;

namespace weather.Services
{
    public interface IOpenWeatherService
    {
        Task<WeatherDto> GetWeather(string cityName);

        Task<WeatherDto> GetWeather(int cityId);
    }
}
