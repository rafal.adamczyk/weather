﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using weather.Data.Dtos;

namespace weather.Services
{
    public class OpenWeatherService : IOpenWeatherService
    {
        private static readonly JsonSerializerSettings SnakeCase = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, ContractResolver = new DefaultContractResolver { NamingStrategy = new SnakeCaseNamingStrategy() } };
        
        private readonly Uri _baseUri = new Uri("https://api.openweathermap.org/data/2.5/weather?APPID=134c4f0e96e86213782afc88cc7d1703&units=metric");
        private readonly HttpClient _httpClient = new HttpClient();

        public async Task<WeatherDto> GetWeather(string cityName)
        {
            var uriBuilder = new UriBuilder(_baseUri);
            uriBuilder.Query = uriBuilder.Query.Substring(1) + "&" + $"q={cityName}";

            return await GetWeather(uriBuilder.Uri);
        }

        public async Task<WeatherDto> GetWeather(int cityId)
        {
            var uriBuilder = new UriBuilder(_baseUri);
            uriBuilder.Query = uriBuilder.Query.Substring(1) + "&" + $"id={cityId}";

            return await GetWeather(uriBuilder.Uri);
        }

        private async Task<WeatherDto> GetWeather(Uri requestUri)
        {
            HttpRequestMessage getWeatherRequest = new HttpRequestMessage(HttpMethod.Get, requestUri);
            HttpResponseMessage response = await _httpClient.SendAsync(getWeatherRequest);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Error {response.StatusCode}\n{response.ReasonPhrase}");
            }

            string content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<WeatherDto>(content, SnakeCase);
        }
    }
}