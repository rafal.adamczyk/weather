﻿namespace weather.Data.Dtos
{
    public class WeatherDto
    {
        public MainWeatherData Main { get; set; }

        public class MainWeatherData
        {
            public double Temp { get; set; }

            public double Pressure { get; set; }

            public double Humidity { get; set; }
        }
    }
}