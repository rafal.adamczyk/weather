﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace weather.Data.Models
{
    public class City
    {
        public int Id { get; set; }

        [Required, DisplayName("Nazwa")]
        public string Name { get; set; }

        [Required]
        public int OpenWeartherId { get; set; }
    }
}