﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using weather.Data.Models;

namespace weather.Data
{
    public static class Seeder
    {
        public static IWebHost Seed(this IWebHost host)
        {
            using (IServiceScope scope = host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                dbContext.Database.Migrate();
                AddOrUpdateCities(dbContext, new[]
                {
                    new City {Name = "Wrocław", OpenWeartherId = 3081368},
                    new City {Name = "Warszawa", OpenWeartherId = 756135},
                    new City {Name = "London", OpenWeartherId = 2643743},
                    new City {Name = "Kuala Lumpur", OpenWeartherId = 1733046},
                });
            }

            return host;
        }

        private static void AddOrUpdateCities(ApplicationDbContext dbContext, IEnumerable<City> cities)
        {
            foreach (var city in cities)
            {
                var existingCity = dbContext.Cities.SingleOrDefault(c => c.OpenWeartherId == city.OpenWeartherId && c.Name == city.Name);
                if (existingCity != null)
                {
                    existingCity.Name = city.Name;
                    existingCity.OpenWeartherId = city.OpenWeartherId;
                }
                else
                {
                    dbContext.Add(city);
                }
            }

            dbContext.SaveChanges();
        }
    }
}
